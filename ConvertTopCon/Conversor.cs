﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace ConvertTopCon
{
    public partial class Conversor : Form
    {
        Execute Converter = new Execute();
        About Sobre = new About();

        public Conversor()
        {
            InitializeComponent();
            FDEntrada.Filter = "Arquivos Tps (tps)|*.tps";
            FDSaida.Filter = "Arquivos 10n (10n)|*.10n";
        }

        private void Entrada_Click(object sender, EventArgs e)
        {
            if (Pastas.Checked)
            {
                BDEntrada.ShowDialog();
                TBEntrada.Text = BDEntrada.SelectedPath;
                if (TBSaida.Text.Trim() == "")
                {
                    TBSaida.Text = BDEntrada.SelectedPath;
                    BDSaida.SelectedPath = BDEntrada.SelectedPath;
                }
            }
            else
            {
                FDEntrada.ShowDialog();
            }
        }

        private void Saida_Click(object sender, EventArgs e)
        {
            if ((Pastas.Checked) || ((!Pastas.Checked) && (FDEntrada.FileNames.Length > 1)))
            {
                BDSaida.ShowDialog();
                TBSaida.Text = BDSaida.SelectedPath;
            }
            else
            {
                FDSaida.ShowDialog();
            }
        }
        
        private void Pastas_CheckedChanged(object sender, EventArgs e)
        {
            if (Pastas.Checked)
            {
                if (TBEntrada.Text.LastIndexOf('.') > 0)
                {
                    if (FDEntrada.FileNames.Length == 1)
                    {
                        TBEntrada.Text = TBEntrada.Text.Substring(0, TBEntrada.Text.LastIndexOf('\\'));
                        BDEntrada.SelectedPath = TBEntrada.Text;
                    }
                    else if (FDEntrada.FileNames.Length > 1)
                    {
                        TBEntrada.Text = FDEntrada.FileNames[0].Substring(0, FDEntrada.FileNames[0].LastIndexOf('\\'));
                    }
                    BDEntrada.SelectedPath = TBEntrada.Text;
                }
                if (TBSaida.Text.LastIndexOf('.') > 0)
                {
                    TBSaida.Text = TBSaida.Text.Substring(0, TBSaida.Text.LastIndexOf('\\'));
                    BDSaida.SelectedPath = TBSaida.Text;
                }
            }
            else
            {
                if (TBEntrada.Text.LastIndexOf('.') < 0)
                {
                    if (FDEntrada.FileNames.Length >0)
                    {
                        TBEntrada.Text = "";
                        foreach (String Nome in FDEntrada.FileNames)
                        {
                            if (TBEntrada.Text.Trim() != "")
                                TBEntrada.Text += ", ";
                            TBEntrada.Text += Nome;
                        }
                    }
                }
                if (TBSaida.Text.LastIndexOf('.') < 0)
                {
                    if (FDEntrada.FileNames.Length == 1)
                    {
                        if (TBSaida.Text.Trim() == "")
                        {
                            TBSaida.Text = FDEntrada.FileName.Substring(0, FDEntrada.FileName.LastIndexOf('.')) + ".10n";
                        }
                        else 
                        {
                            String tmp = FDEntrada.FileName.Substring(FDEntrada.FileName.LastIndexOf('\\'));
                            TBSaida.Text += tmp.Substring(0, tmp.LastIndexOf('.')) + ".10n";
                        }
                        FDSaida.FileName = TBSaida.Text;
                    }
                }
            }
            Estrutura.Enabled = Pastas.Checked;
        }

        private void FDEntrada_FileOk(object sender, CancelEventArgs e)
        {
            TBEntrada.Text = "";
            foreach (String Nome in FDEntrada.FileNames)
            {
                if(TBEntrada.Text.Trim() != "")
                    TBEntrada.Text += ", ";
                TBEntrada.Text += Nome;
            }
            if (FDEntrada.FileNames.Length > 1)
            {
                if (TBSaida.Text.Trim() != "")
                {
                    if (TBSaida.Text.LastIndexOf('.') >= 0)
                    {
                        TBSaida.Text = TBSaida.Text.Substring(0, TBSaida.Text.LastIndexOf('\\'));
                        BDSaida.SelectedPath = TBSaida.Text;
                    }
                }
                else
                {
                    TBSaida.Text = FDEntrada.FileNames[0].Substring(0, FDEntrada.FileNames[0].LastIndexOf('\\'));
                    BDSaida.SelectedPath = TBSaida.Text;
                }
            }
            else if (FDEntrada.FileNames.Length == 1)
            {
                if (TBSaida.Text.Trim() != "")
                {
                    if (TBSaida.Text.LastIndexOf('.') < 0)
                    {
                        TBSaida.Text = FDEntrada.FileNames[0].Substring(FDEntrada.FileNames[0].LastIndexOf('\\') + 1);
                        TBSaida.Text = BDSaida.SelectedPath + "\\" + TBSaida.Text.Substring(0, TBSaida.Text.LastIndexOf('.')) + ".10n";
                        FDSaida.FileName = TBSaida.Text;
                    }
                    else
                    {
                        TBSaida.Text = TBSaida.Text.Substring(0, TBSaida.Text.LastIndexOf('\\'));
                        BDSaida.SelectedPath = TBSaida.Text;
                        String tmp = FDEntrada.FileNames[0].Substring(FDEntrada.FileNames[0].LastIndexOf('\\')+1);
                        TBSaida.Text += "\\" + tmp.Substring(0, tmp.LastIndexOf('.')) + ".10n";
                        FDSaida.FileName = TBSaida.Text;
                    }
                }
                else
                {
                    TBSaida.Text = FDEntrada.FileNames[0].Substring(0, FDEntrada.FileNames[0].LastIndexOf('.')) + ".10n";
                    FDSaida.FileName = TBSaida.Text;
                }
            }
        }

        private void FDSaida_FileOk(object sender, CancelEventArgs e)
        {
            TBSaida.Text = FDSaida.FileNames[0];
        }
        
        private void Executar_Click(object sender, EventArgs e)
        {
            if (Pastas.Checked)
            {
                if (BDEntrada.SelectedPath == "")
                    MessageBox.Show("Escolha uma pasta a ser convertida");
                else if (BDSaida.SelectedPath == "")
                    MessageBox.Show("Escolha a pasta para salvar os arquivo de saída");
                else
                {
                    ConverterPastas(BDEntrada.SelectedPath);
                    MessageBox.Show("Concluido!");
                }
            }
            else if (FDEntrada.FileNames.Length == 1)
            {
                if (FDEntrada.FileName == "")
                    MessageBox.Show("Escolha um arquivo a ser convertido");
                else if (FDSaida.FileName == "")
                    MessageBox.Show("Escolha um arquivo de saída");
                else
                {
                    ConverterArquivo(FDEntrada.FileName, FDSaida.FileName);
                    MessageBox.Show("Concluido!");
                }
            }
            else
            {
                if (FDEntrada.FileNames.Length == 0)
                    MessageBox.Show("Escolha algum arquivo a ser convertido");
                else if (BDSaida.SelectedPath == "")
                    MessageBox.Show("Escolha a pasta para salvar os arquivo de saída");
                else
                {
                    String Pasta;
                    Pasta = FDEntrada.FileNames[0];
                    Pasta = Pasta.Substring(0, Pasta.LastIndexOf('\\'));
                    ConverterLista(FDEntrada.FileNames, Pasta);
                    MessageBox.Show("Concluido!");
                }

            }
        }

        public void ConverterPastas(String Pasta)
        {
            ConverterPasta(Pasta);
            String[] Nomes;
            Nomes = Directory.GetDirectories(Pasta);
            foreach (String Nome in Nomes)
            {
                ConverterPastas(Nome);
            }
        }
        public void ConverterPasta(String Pasta)
        {
            String[] Nomes;
            Nomes = Directory.GetFiles(Pasta, "*.tps");
            ConverterLista(Nomes, Pasta);

        }
        public void ConverterLista(String[] Nomes,String Pasta) 
        {
            String Arquivo, Saida = BDSaida.SelectedPath;
            if ((Estrutura.Checked) && (Pasta.Substring(BDEntrada.SelectedPath.Length).Trim() != ""))
            {
                Saida += Pasta.Substring(BDEntrada.SelectedPath.Length);
                Directory.CreateDirectory(Saida);
            }
            foreach (String Nome in Nomes)
            {
                Arquivo = Nome.Substring(Nome.LastIndexOf('\\'));
                Arquivo = Saida + Arquivo;
                ConverterArquivo(Nome, Arquivo);
            }
        }
        public int ConverterArquivo(String Arquivo)
        {
            return ConverterArquivo(Arquivo, Arquivo);
        }
        public int ConverterArquivo(String Arquivo, String Salvar)
        {

            if (Arquivo.LastIndexOf('.') > 0)
            {
                Arquivo = Arquivo.Substring(0, Arquivo.LastIndexOf('.'));
            }

            if (Salvar.LastIndexOf('.') > 0)
            {
                Salvar = Salvar.Substring(0, Salvar.LastIndexOf('.'));
            }

            int i = 1;
            String Tmp = Salvar;
            while (File.Exists(Tmp + ".10n") || File.Exists(Tmp + ".10o"))
            {
                Tmp = Salvar + "_" + i;
                i++;
            }
            Salvar = Tmp;
            System.Console.WriteLine(Arquivo);
            System.Console.WriteLine(Salvar);
            Arquivo = "teqc +nav \"" + Salvar + ".10n\" -top tps \"" + Arquivo + ".tps\">\"" + Salvar + ".10o\"";
            System.Console.WriteLine(Arquivo);
            return Converter.ExecuteCommand(Arquivo);
        }
        private void PBSobre_Click(object sender, EventArgs e)
        {
            Sobre.ShowDialog();
        }


    }
}
