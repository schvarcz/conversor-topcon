﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace ConvertTopCon
{
    class Execute
    {
        public String[] arquivos;
        public int ExecuteCommand(string Command)
        {
            int ExitCode;
            ProcessStartInfo ProcessInfo;
            Process Process;

            ProcessInfo = new ProcessStartInfo("cmd.exe", "/C " + Command);
            ProcessInfo.CreateNoWindow = true;
            ProcessInfo.UseShellExecute = false;
            Process = Process.Start(ProcessInfo);
            //Process.WaitForInputIdle();
            Process.WaitForExit();
            ExitCode = Process.ExitCode;
            Process.Close();

            return ExitCode;
        }
    }
}
