﻿namespace ConvertTopCon
{
    partial class Conversor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Conversor));
            this.Entrada = new System.Windows.Forms.Button();
            this.FDEntrada = new System.Windows.Forms.OpenFileDialog();
            this.Executar = new System.Windows.Forms.Button();
            this.Saida = new System.Windows.Forms.Button();
            this.TBEntrada = new System.Windows.Forms.TextBox();
            this.TBSaida = new System.Windows.Forms.TextBox();
            this.Estrutura = new System.Windows.Forms.CheckBox();
            this.Pastas = new System.Windows.Forms.CheckBox();
            this.Opcao = new System.Windows.Forms.GroupBox();
            this.FDSaida = new System.Windows.Forms.SaveFileDialog();
            this.BDEntrada = new System.Windows.Forms.FolderBrowserDialog();
            this.BDSaida = new System.Windows.Forms.FolderBrowserDialog();
            this.LBSaida = new System.Windows.Forms.Label();
            this.LBEntrada = new System.Windows.Forms.Label();
            this.PBSobre = new System.Windows.Forms.PictureBox();
            this.Opcao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PBSobre)).BeginInit();
            this.SuspendLayout();
            // 
            // Entrada
            // 
            this.Entrada.Location = new System.Drawing.Point(271, 35);
            this.Entrada.Name = "Entrada";
            this.Entrada.Size = new System.Drawing.Size(31, 27);
            this.Entrada.TabIndex = 0;
            this.Entrada.Text = "...";
            this.Entrada.UseVisualStyleBackColor = true;
            this.Entrada.Click += new System.EventHandler(this.Entrada_Click);
            // 
            // FDEntrada
            // 
            this.FDEntrada.Multiselect = true;
            this.FDEntrada.FileOk += new System.ComponentModel.CancelEventHandler(this.FDEntrada_FileOk);
            // 
            // Executar
            // 
            this.Executar.Location = new System.Drawing.Point(120, 205);
            this.Executar.Name = "Executar";
            this.Executar.Size = new System.Drawing.Size(87, 27);
            this.Executar.TabIndex = 1;
            this.Executar.Text = "Executar";
            this.Executar.UseVisualStyleBackColor = true;
            this.Executar.Click += new System.EventHandler(this.Executar_Click);
            // 
            // Saida
            // 
            this.Saida.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Saida.Location = new System.Drawing.Point(271, 93);
            this.Saida.Name = "Saida";
            this.Saida.Size = new System.Drawing.Size(31, 27);
            this.Saida.TabIndex = 2;
            this.Saida.Text = "...";
            this.Saida.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.Saida.UseVisualStyleBackColor = true;
            this.Saida.Click += new System.EventHandler(this.Saida_Click);
            // 
            // TBEntrada
            // 
            this.TBEntrada.Location = new System.Drawing.Point(23, 38);
            this.TBEntrada.Name = "TBEntrada";
            this.TBEntrada.ReadOnly = true;
            this.TBEntrada.Size = new System.Drawing.Size(240, 23);
            this.TBEntrada.TabIndex = 3;
            // 
            // TBSaida
            // 
            this.TBSaida.Location = new System.Drawing.Point(23, 97);
            this.TBSaida.Name = "TBSaida";
            this.TBSaida.ReadOnly = true;
            this.TBSaida.Size = new System.Drawing.Size(240, 23);
            this.TBSaida.TabIndex = 3;
            // 
            // Estrutura
            // 
            this.Estrutura.AutoSize = true;
            this.Estrutura.Enabled = false;
            this.Estrutura.Location = new System.Drawing.Point(17, 48);
            this.Estrutura.Name = "Estrutura";
            this.Estrutura.Size = new System.Drawing.Size(140, 17);
            this.Estrutura.TabIndex = 4;
            this.Estrutura.Text = "Criar estrutura de pastas";
            this.Estrutura.UseVisualStyleBackColor = true;
            // 
            // Pastas
            // 
            this.Pastas.AutoSize = true;
            this.Pastas.Location = new System.Drawing.Point(9, 22);
            this.Pastas.Name = "Pastas";
            this.Pastas.Size = new System.Drawing.Size(241, 17);
            this.Pastas.TabIndex = 4;
            this.Pastas.Text = "Converter arquivos de uma estrutura de pasta";
            this.Pastas.UseVisualStyleBackColor = true;
            this.Pastas.CheckedChanged += new System.EventHandler(this.Pastas_CheckedChanged);
            // 
            // Opcao
            // 
            this.Opcao.Controls.Add(this.Estrutura);
            this.Opcao.Controls.Add(this.Pastas);
            this.Opcao.Location = new System.Drawing.Point(14, 127);
            this.Opcao.Name = "Opcao";
            this.Opcao.Size = new System.Drawing.Size(303, 76);
            this.Opcao.TabIndex = 5;
            this.Opcao.TabStop = false;
            this.Opcao.Text = "Opções";
            // 
            // FDSaida
            // 
            this.FDSaida.FileOk += new System.ComponentModel.CancelEventHandler(this.FDSaida_FileOk);
            // 
            // LBSaida
            // 
            this.LBSaida.AutoSize = true;
            this.LBSaida.Location = new System.Drawing.Point(24, 80);
            this.LBSaida.Name = "LBSaida";
            this.LBSaida.Size = new System.Drawing.Size(109, 15);
            this.LBSaida.TabIndex = 6;
            this.LBSaida.Text = "Caminho de saida:";
            // 
            // LBEntrada
            // 
            this.LBEntrada.AutoSize = true;
            this.LBEntrada.Location = new System.Drawing.Point(22, 21);
            this.LBEntrada.Name = "LBEntrada";
            this.LBEntrada.Size = new System.Drawing.Size(160, 15);
            this.LBEntrada.TabIndex = 7;
            this.LBEntrada.Text = "Caminho dos arquivos *.tps";
            // 
            // PBSobre
            // 
            this.PBSobre.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.PBSobre.Image = ((System.Drawing.Image)(resources.GetObject("PBSobre.Image")));
            this.PBSobre.Location = new System.Drawing.Point(306, 215);
            this.PBSobre.Name = "PBSobre";
            this.PBSobre.Size = new System.Drawing.Size(28, 22);
            this.PBSobre.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PBSobre.TabIndex = 9;
            this.PBSobre.TabStop = false;
            this.PBSobre.Click += new System.EventHandler(this.PBSobre_Click);
            // 
            // Conversor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(331, 237);
            this.Controls.Add(this.PBSobre);
            this.Controls.Add(this.LBEntrada);
            this.Controls.Add(this.LBSaida);
            this.Controls.Add(this.TBSaida);
            this.Controls.Add(this.TBEntrada);
            this.Controls.Add(this.Saida);
            this.Controls.Add(this.Executar);
            this.Controls.Add(this.Entrada);
            this.Controls.Add(this.Opcao);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Conversor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Conversor Tps";
            this.Opcao.ResumeLayout(false);
            this.Opcao.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PBSobre)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Entrada;
        private System.Windows.Forms.OpenFileDialog FDEntrada;
        private System.Windows.Forms.Button Executar;
        private System.Windows.Forms.Button Saida;
        private System.Windows.Forms.TextBox TBEntrada;
        private System.Windows.Forms.TextBox TBSaida;
        private System.Windows.Forms.CheckBox Estrutura;
        private System.Windows.Forms.CheckBox Pastas;
        private System.Windows.Forms.GroupBox Opcao;
        private System.Windows.Forms.SaveFileDialog FDSaida;
        private System.Windows.Forms.FolderBrowserDialog BDEntrada;
        private System.Windows.Forms.FolderBrowserDialog BDSaida;
        private System.Windows.Forms.Label LBSaida;
        private System.Windows.Forms.Label LBEntrada;
        private System.Windows.Forms.PictureBox PBSobre;
    }
}

